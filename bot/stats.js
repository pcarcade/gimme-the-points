const {By} = require('selenium-webdriver');
const {logError, logStats} = require('./logger')
const {elementIsDisplayed, withDelay, ensureUrl} = require('./helpers')
const statsUrl = 'https://rewards.microsoft.com/Signin?idru=/?refd=www.bing.com&redref=amc'

const getLevel = async function ()  {
  try {
    await elementIsDisplayed(By.className('banner-padded-content'))
    return await driver.executeScript(`return document.querySelector('.level').textContent`)
  } catch (error) {
    logError('getLevel error:')
    logError(error)
  }
}

//probably add some delay...
const ensureDashboardElement = async function () { 
  let banner = await driver.findElements(By.className('banner-padded-content')), tryTimes = 0
  if (banner.length) return
  while (!banner.length || tryTimes <= 3) {
    await driver.navigate().refresh()
    await withDelay(2500)
    banner = await driver.findElements(By.className('banner-padded-content'))
    tryTimes++
  }
}

const splitPoints = async function (points) {
  const splitPoints = points.split('/')
  return {have:parseInt(splitPoints[0]), need:parseInt(splitPoints[1])}
}

const getStreak = async function () {
  return await driver.executeScript("return document.querySelectorAll('.title-detail')[1].children[0].innerText")
}

const getAvailablePoints = async function () {
  return await driver.executeScript("return document.querySelectorAll('.title-detail')[0].children[0].innerText")
}

const getLifetimePoints = async function () {
  const pointsRegEx = /\d{1,3}(,\d{3})*/
  const pointsString = await driver.executeScript("return document.querySelectorAll('.title-detail')[0].children[2].innerText")
  return pointsString.match(pointsRegEx)[0]
}

const getPointsEarned = async function () {
  return await driver.executeScript("return document.querySelectorAll('.title-detail')[2].children[0].innerText")
} 

const getPcPoints = async function (stats) {
  let pcPoints = await driver.executeScript("return document.querySelectorAll('.pointsDetail')[0].children[0].innerText")
  pcPoints =  await splitPoints(pcPoints)
  stats.points.pc = pcPoints.have
  stats.maxPoints.pc = pcPoints.need
  return stats
}

const getMobilePoints = async function (stats) {
  let mobilePoints = await driver.executeScript("return document.querySelectorAll('.pointsDetail')[1].children[0].innerText")
  mobilePoints = await splitPoints(mobilePoints)
  stats.points.mobile = mobilePoints.have
  stats.maxPoints.mobile = mobilePoints.need
  return stats
}

const setLevelOneStats = async function (stats) {
  stats.points.edgeBonus = await driver.executeScript("return document.querySelectorAll('.pointsDetail')[1].children[0].children[0].innerText")
  stats.points.mobile = null
  return stats
}

const setLevelTwoStats = async function (stats) {
  stats = await getMobilePoints(stats)
  stats.points.edgeBonus = await driver.executeScript("return document.querySelectorAll('.pointsDetail')[2].children[0].children[0].innerText")
  return stats
}

const getStats = async function (log) {
  await ensureUrl(statsUrl, '\/rewards\/|rewards-dashboard|rewards\.microsoft')
  await ensureDashboardElement()
  let stats = {level:await getLevel(), points:{}, maxPoints:{}}
  stats = await getPcPoints(stats)
  if (/2/.test(stats.level)) stats = await setLevelTwoStats(stats)
  if (/1/.test(stats.level)) stats = await setLevelOneStats(stats)
  if (log) logStats(`--${stats.level}, PC points:${stats.points.pc}, Mobile Points:${stats.points.mobile}, Edge Bonus:${stats.points.edgeBonus}`)
  return  stats 
} 

const getAccountStats = async function () {
  await driver.get('https://rewards.microsoft.com/status/')
  await withDelay(2000)
  await signInButtonCheckMobile()
  await withDelay(2000)
  await ensureUrl(statsUrl, '\/rewards\/|rewards-dashboard|rewards\.microsoft')
  await ensureDashboardElement()
  logStats(`\n*************\nStreak:${await getStreak()}.\nAvailable Points:${await getAvailablePoints()}.\nLifetime Points:${await getLifetimePoints()}.\n*************\n`)
}

const signInButtonCheckMobile = async function () {
  const signInBtn = await driver.findElements(By.id('raf-signin-link-id'))
  if (signInBtn.length) {
    if (await signInBtn[0].isDisplayed()) {
      await signInBtn[0].click()
    }
  }
}

module.exports = {
  getStats,
  getAccountStats
}

