const fetch = require('node-fetch')
const {withDelay} = require('./helpers')
const {simpleQuizCheck, rewardsQuizCheck, pollCheck, simpleQuizStart, rewardsQuizStart, pollStart, whatsOnTopStart} = require('./quiz')
const {logError, logStats} = require('./logger')

const getRedditPosts  = async function () {
  const redditPosts = await fetch('https://www.reddit.com/r/MicrosoftRewards/search.json?sort=new&restrict_sr=on&q=flair%3AMail%2BPoints')
  if (redditPosts.status != 200) throw new Error('Unable to retrieve reddit posts.')
  return await redditPosts.json()
}

const getEmailLinks = async function () {
  return await filterEmailLinks(await getRedditPosts())
}

const filterEmailLinks = async function (redditPosts) {
  const links = []
  redditPosts.data.children.forEach(post => {
    //only get posts from last month
    if (((new Date().getTime() - 1000 * 60 * 60 * 24) - post.data.created_utc * 1000) > 0) return
    let linkPost = [...post.data.selftext.matchAll(/(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)/gi)]
    linkPost.forEach(link => {
      if(link[0].match("aka.ms"||"e.microsoft")) links.push(link[0])
    });
  });
  console.log(links)
  return links
}



const cycleEmailLinks = async function (links) {
  for (let index = 0; index < links.length; index++) {
    const link = links[index];
    try {
      await driver.get(link)
      const url = await driver.getCurrentUrl()
      await withDelay(2500)
      if (await pollCheck(url)) await pollStart()
      if (await rewardsQuizCheck(url) === 'rq')  await rewardsQuizStart()
      if (await rewardsQuizCheck(url) === 'wot')  await whatsOnTopStart()
      if (await simpleQuizCheck(url)) await simpleQuizStart()
      await withDelay(2500)
    } catch (error) {
      logError(error)
    }
  }
}

const runEmailLinks = async function () {
  try {
    await logStats('--attempting to run email links...')
    await cycleEmailLinks(await getEmailLinks())
    await logStats('--email links done.')
  } catch (error) {
    logError(error)
  }
}

module.exports = {runEmailLinks}