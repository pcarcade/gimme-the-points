const { getQueries } = require('./queries')
const { By, Key } = require('selenium-webdriver');
const { withDelay, elementIsDisplayed, forceClick, randSeconds, ensureUrl, ensureSignedIn, acceptTerms, looksGoodInfo } = require('./helpers')
const { logError, logStats } = require('./logger')
const { getStats } = require('./stats')
const args = require('minimist')(process.argv.slice(2))
const config = require('../config/config.json')
const searchUrl = 'https://www.bing.com'



const searchPoints = async function (stats, type) {
  let timesRan = 0
  while (timesRan < config.searchRuns) {
    try {
      if (isPointsAchieved(stats, type)) break;

      const searchesNeeded = getSearchesNeeded(stats, type)
      const extraSearches = getExtraSearches()

      logSearchGoal(stats, type, extraSearches)

      await goBingHome()
      await search((searchesNeeded + extraSearches), type)

      stats = await getStats()
      timesRan++
    } catch (error) {
      logError('Search Points error: ' + error.name)
      logError(error)
    }
  }
  logSearchStats(stats, type, timesRan)
}

const search = async function (searchesNeeded, type) {
  const searches = await getQueries(searchesNeeded)
  await driver.get(searchUrl)
  await ensureSignedIn()
  await withDelay(2000)
  await ensureUrl(searchUrl, '.bing.')
  await acceptTerms(By.id('bnp_btn_accept'))
  console.log(searches)
  await homeSearch(searches[0], type)
  await ensureSignedIn()
  await searchRest(searches)
}

const homeSearch = async function (searchTerm, type) {
  const searchBtnName = getSearchBtnName(type)
  const pcSearchBtn = await driver.findElements(By.id('sb_form_q'))
  const mobileSearchBtn = await driver.findElements(By.className('sbBtn'))
  try {
    if (!pcSearchBtn.length && !mobileSearchBtn.length) await goBingHome()
    await dismissAlert()
    await withDelay(randSeconds())
    await driver.findElement(By.id('sb_form_q')).sendKeys(searchTerm, Key.RETURN)
    await withDelay(150)
    await dismissAlert()
    if (!await urlCheck()) await driver.findElement(By.className(searchBtnName.name)).click()
    await dismissAlert()
    if (!await urlCheck()) await forceClick(searchBtnName.pre + searchBtnName.name)
    await withDelay(randSeconds())
    await dismissAlert()
    if (!await urlCheck()) await driver.findElement(By.id(searchBtnName)).click()
    await withDelay(randSeconds())
  } catch (error) {
    if (error.name == 'StaleElementReferenceError') return
    logError('home search error')
    logError(error)
  }
}

const searchRest = async function (searches) {
  await looksGoodInfo()
  await ensureSearched()
  await acceptTerms(By.id('bnp_btn_accept'))
  await withDelay(randSeconds())

  try {
    for (let index = 1; index < searches.length; index++) {
      await dismissAlert()
      await looksGoodInfo()
      await acceptTerms(By.id('bnp_btn_accept'))
      await elementIsDisplayed(By.className('b_searchboxSubmit'))
      await withDelay(randSeconds())
      await dismissAlert()
      await driver.findElement(By.id('sb_form_q')).sendKeys(Key.CONTROL, 'a', Key.DELETE)
      await withDelay(randSeconds())
      await dismissAlert()
      await driver.findElement(By.id('sb_form_q')).sendKeys(searches[index])
      await withDelay(randSeconds())
      await dismissAlert()
      await driver.findElement(By.className('b_searchboxSubmit')).click()
      await withDelay(randSeconds())
      await elementIsDisplayed(By.className('b_searchboxSubmit'))
    }
  } catch (error) {
    logError('search rest error')
    logError(error)
  }
}

const searchesNeededPc = function (maxPoints, gainedPoints) {
  if (maxPoints == 150 || maxPoints == 300 || maxPoints == 50 || maxPoints == 100) return (maxPoints - gainedPoints) / 5
  return (maxPoints - gainedPoints) / 3
}

const searchesNeededMobile = function (maxPoints, gainedPoints) {
  if (maxPoints == 100 || maxPoints == 200) return (maxPoints - gainedPoints) / 5
  return (maxPoints - gainedPoints) / 3
}

const logSearchGoal = function (stats, type, extraSearches) {
  if (type === 'pc') {
    console.log(`Account ${stats.level}. ${searchesNeeded} searches need to be made for max ${stats.maxPoints.pc}` +
      ` search points & ${extraSearches} extra searches. Yea boiiiiii.`)
  }

  if (type === 'mobile') {
    console.log(`Account ${stats.level}. Lets do ${searchesNeeded} mobile searches & ${extraSearches}` +
      ` extra searches. Skkrr Skkrr `)
  }
}

const getSearchBtnName = function (type) {
  if (type === 'pc') return { pre: '.', name: 'search' }
  if (type === 'mobile') return { pre: '#', name: 'sbBtn' }
}

const logSearchStats = async function (stats, type, timesRan) {
  if (type === 'pc') {
    logStats(`--Ran PC search ${timesRan} times and achieved ${stats.points.pc}/${stats.maxPoints.pc} points.`)
  }

  if (type === 'mobile') {
    logStats(`--Ran Mobile search ${timesRan} times and achieved ${stats.points.mobile}/${stats.maxPoints.mobile} points.`)
  }
}

const isPointsAchieved = function (stats, type) {
  if (!stats.points) throw new Error ('Unable to retrieve rewards points.')
  if (type === 'pc') {
    if (stats.points.pc == stats.maxPoints.pc) return true
  }

  if (type === 'mobile') {
    if (stats.points.mobile == stats.maxPoints.mobile) return true
  }
  return false
}

const getSearchesNeeded = function (stats, type) {
  if (type === 'pc') return searchesNeeded = searchesNeededPc(stats.maxPoints.pc, stats.points.pc)
  if (type === 'mobile') return searchesNeeded = searchesNeededMobile(stats.maxPoints.mobile, stats.points.mobile)
}

const getExtraSearches = function () {
  if (args.extrasearches) return Math.floor(Math.random() * 11)
  return 0
}

const dismissAlert = async function () {
  try {
    const alert = await driver.switchTo().alert()
    await alert.dismiss()
    await withDelay(randSeconds())
  } catch (e) { }
}

const goBingHome = async function () {
  try {
    await driver.get(searchUrl)
    await ensureUrl(searchUrl, '.bing.')
  } catch (error) {
    throw new Error ('Unable to get to bing homepage.')
  }
  
}

const urlCheck = async function () {
  return /\/search\?q/.test(await driver.getCurrentUrl())
}

const ensureSearched = async function () {
  const userAgent = await driver.executeScript('return navigator.userAgent')
  const searchField = await driver.findElements(By.className('b_searchboxSubmit'))
  let tryTimes = 0
  if (searchField.length) return
  while (!searchField.length && tryTimes <= 3) {
    console.log('trying ensure searched', tryTimes)
    await driver.get(searchUrl)
    await withDelay(3000)
    if (/Phone/.test(userAgent)) {
      const mobileSearchBtn = await driver.findElements(By.id('sbBtn'))
      if (mobileSearchBtn.length) {
        await mobileSearchBtn[0].click()
        break
      }
      break
    }
    const searchBtn = await driver.findElements(By.className('search'))
    if (searchBtn.length) {
      await searchBtn[0].click()
      break
    }
    tryTimes++
  }
}

const searchTest = async function (searchesNeeded) {
  await goBingHome()
  await search(searchesNeeded)
}

module.exports = { searchTest, searchPoints }
