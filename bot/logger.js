const fs = require('fs')
const log4js = require('log4js');
let fileDate = () => {
  return (new Date().toJSON()).slice(0,10)
}

function createLogFile () {
  log4js.configure({
    appenders: {
      info: { type: 'dateFile', filename: `./logs/${fileDate()}-info.log`, keepFileExt: true, level:'info'},
      error: { type: 'dateFile', filename: `./logs/${fileDate()}-error.log`, keepFileExt: true , level:'error'},
      debug: { type: 'file', filename: `./logs/debug.log`, keepFileExt: true , level:'debug'}
    },
    categories: {
      default: {appenders: ['info'], level:'info' },
      error: {appenders: ['error'], level:'error' },
      debug: {appenders: [ 'info','error','debug'], level: 'debug'}
    }
  });
}

const logError = (errorMsg) => {
  let logger = log4js.getLogger('error')
  console.error(errorMsg)
  //process.stderr.write('\r\n' + errorMsg)
  logger.error(errorMsg)

}

const logStats = (msg) => {
  logger = log4js.getLogger('info')
  console.log(msg)
  //process.stdout.write('\r\n' + msg)
  logger.info(msg)
}

const logDebug = ({msg,errmsg}) => {
  let logger = log4js.getLogger('debug')
  if (msg) zz = logger.debug(msg)
  if (errmsg) zz = logger.debug(errmsg)
}



module.exports = {
  logError,
  logStats,
  createLogFile
}