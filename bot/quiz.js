const { randSeconds, withDelay, elementToExist, elementIsDisplayed, elementIsEnabled, closeTab } = require('./helpers')
const { By, until } = require('selenium-webdriver');
const { logError, logStats } = require('./logger')

const pollCheck = async function (url) {
  try{
    await driver.wait(until.elementLocated(By.id('btoption0')), 2000)
  } catch (err) {}
  const pollOption = await driver.findElements(By.id('btoption0'))
  if (pollOption.length) return true
  else return false
}

const pollStart = async function () {
  await elementIsDisplayed(By.id('btoption0'))
  const choice = ['btoption0', 'btoption1']
  const element = await driver.findElement(By.id(choice[Math.floor(Math.random() * 2)]))
  await withDelay(randSeconds())
  await element.click()
  await withDelay(randSeconds())
}

const simpleQuizCheck = async function (url) {
  if (/uiz&rnorewar/.test(url) || /quiz&setlang/.test(url)) return true
  return false
}

async function rewardsOverlayClose() {
  const rewardsOverlay = await driver.findElements(By.id('rqCloseBtn'))
  if (rewardsOverlay.length) {
    if (await rewardsOverlay[0].isDisplayed()) return driver.findElement(By.id('rqCloseBtn')).click()
  }
  await withDelay(randSeconds())
}

const simpleQuizStart = async function () {
  await rewardsOverlayClose()
  try {
    console.log('simple quiz running')
    await elementIsDisplayed(By.className('wk_Circle'))
    let quizLength = await driver.findElement(By.className('FooterText0')).getText()
    quizLength = quizLength.match(/(\d+)(?!.*\d)/g)
    console.log(`detected quiz length of`, quizLength)
    for (let index = 0; index < quizLength; index++) {
      const choice = Math.floor(Math.random() * 3)
      await driver.findElement(By.id(`ChoiceText_${index}_${choice}`)).click()
      await withDelay(randSeconds())
      const submitBtn = await driver.findElements(By.name('submit'))
      if (submitBtn.length) {
        if (!await submitBtn[0].isDisplayed()) return
        await submitBtn[0].click()
        await withDelay(randSeconds())
      }
    }
  } catch (error) {
    logError('simplequiz error:')
    logError(error)
  }
}

const rewardsQuizCheck = async function (url) {
  if (!/REWARDSQUIZ/.test(url) && !/Rewards/.test(url)) return false
  const overlayPanel = await driver.findElements(By.id('overlayPanel'))
  if (overlayPanel.length === 0) return false
  if (/WhatsOnTop/i.test(url) || /q=Bing/.test(url) || /_That_/i.test(url) || /orThat_/i.test(url)) return 'wot'
  return 'rq'
}


const rewardsQuizStart = async function () {
  console.log('starting rewards quiz')
  await elementIsDisplayed(By.id('overlayPanel'))
  if (await resumeQuizCheck()) return await resumeQuiz()
  await startQuiz()
  const quizLength = await getRewardsQuizLength()
  await rewardsQuizRun(quizLength)
}

const startQuiz = async function () {
  await elementToExist(By.id('rqStartQuiz'))
  await elementIsDisplayed(By.id('rqStartQuiz'))
  await elementIsEnabled(By.id('rqStartQuiz'))
  //wait the animation
  await withDelay(1500)
  await driver.findElement(By.id('rqStartQuiz')).click()
}


const getRewardsQuizLength = async function () {
  await elementIsDisplayed(By.id('rqQuestionState1'))
  let quizLength = await driver.findElement(By.id('rqHeaderCredits'))
  quizLength = await quizLength.findElements(By.xpath('.//*'))
  console.log(`Detected RewardsQuiz length of ${quizLength.length}`)
  return quizLength.length
}

const rewardsQuizRun = async function (quizLength) {
  for (let question = 0; question != quizLength; question++) {
    await rewardsQuizAnswer(question)
    await withDelay(500)
    try { await elementIsDisplayed(By.className(`rqPoints`)) } catch (error) { console.log('wait for rqanaswer timeout'+ error) }
    await withDelay(randSeconds())
  }
}

const rewardsQuizAnswer = async function (question) {
  let correctAnswerMsg = false, answer = 0
  while (!correctAnswerMsg) {
    try {
      //console.log(`Question ${question}, answer ${answer}`)
      await withDelay(randSeconds())
      await tryRewardsQuizAnswerBtns(answer)
      answer++
      correctAnswerMsg = await isRewardsQuizCorrectMsg()
      if (answer >= 15) break;
      await withDelay(randSeconds())
    } catch (error) {
      console.log('RewardsQuiz Answer error'+ error)
      break
    }
  }
}

const isRewardsQuizCorrectMsg = async function () {
  try { await elementIsDisplayed(By.id(`rqcorrectAns`), 1000) } catch { }
  let correctAnswerElement = await driver.findElements(By.id('rqcorrectAns'))
  if (correctAnswerElement.length) {
    return correctAnswerMsg = await correctAnswerElement[0].isDisplayed()
  }
}

const tryRewardsQuizAnswerBtns = async function (answer) {
  const answerButtons = await driver.findElements(By.id(`rqAnswerOption${answer}`))
  for (let index = 0; index < answerButtons.length; index++) {
    const button = answerButtons[index];
    if (await button.isDisplayed()) {
      await button.click()
      break
    }
  }
}

const resumeQuizCheck = async function () {
  const quizCircles = await driver.findElements(By.id('rqQuestionState1'))
  if (quizCircles.length) {
    if (await quizCircles[0].isDisplayed()) return true
  }
  return false
}

//works with some error but it goes thru it. need to rework to be similar to new rewards quiz tho
const resumeQuiz = async function () {
  console.log('resuming quiz')
  await withDelay(2500)
  let filledCircles = await driver.findElements(By.className('filledCircle'))
  filledCircles = filledCircles.length - 1
  let quizLength = await getRewardsQuizLength()
  quizLength = quizLength - filledCircles
  await withDelay(2500)
  await rewardsQuizRun(quizLength)
}

const whatsOnTopStart = async function () {
  console.log('Starting whats on top quiz')
  await elementIsDisplayed(By.id('overlayPanel'))
  if (await resumeWhatsOnTopCheck()) return await resumeWhatsOnTop()
  await startQuiz()
  await whatsOnTopAnswer(await getWhatsOnTopLength())
}


const whatsOnTopAnswer = async function (quizLength) {
  console.log(`WhatsOnTop Quiz Length of ${quizLength}`)
  try {
    for (let question = 0; question < quizLength; question++) {
      await elementIsDisplayed(By.id('overlayPanel'))
      await elementIsDisplayed(By.className(`rqPoints`))
      await elementIsDisplayed(By.id(`rqAnswerOption0`))
      await withDelay(randSeconds())
      driver.findElement(By.id(`rqAnswerOption${Math.floor(Math.random() * 2)}`)).click()
      try { await elementIsDisplayed(By.css('.btWrongAns, .btCorrectAns'), 25000) } catch (e) { console.log('incorrect/correct answer display timeout') }
    }
  } catch (error) {
    logError('whatsOnTop answer error:')
    logError(error)
  }
}

const getWhatsOnTopLength = async function () {
  let quizFooter = await driver.findElement(By.className('bt_Quefooter')).getText()
  return quizFooter.match(/(\d+)(?!.*\d)/g)
}

const resumeWhatsOnTopCheck = async function () {
  const quizState = await driver.findElement(By.className('bt_Quefooter'))
  if (await quizState.isDisplayed()) return true
  return false
}

const resumeWhatsOnTop = async function () {
  console.log('Resuming WhatsOnTop Quiz...')
  let quizFooter = await driver.findElement(By.className('bt_Quefooter')).getText()
  let quizLength = quizFooter.match(/(\d+)(?!.*\d)/g) - (quizFooter.match(/(\d+)/) - 1)
  await whatsOnTopAnswer(quizLength)
}


module.exports = {
  simpleQuizCheck,
  pollCheck,
  rewardsQuizCheck,
  simpleQuizStart,
  pollStart,
  rewardsQuizStart,
  whatsOnTopStart
}