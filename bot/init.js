const Bot = require('./bot')
const {withDelay, takeScreenshot, shuffleAccounts} = require('./helpers')
const {logError, logStats} = require('./logger')
const {getStats, getAccountStats} = require('./stats')
const {rewardsQuizCheck, simpleQuizCheck} = require('./quiz')
const args = require('minimist')(process.argv.slice(2))
const {By, until} = require('selenium-webdriver');
const {searchTest} = require('./search_points')
const {checkForPunchcards} = require('./punchcards')

const runAll = async function (accounts, emaillinks) {
  if (args.shuffle) accounts = shuffleAccounts(accounts, 0)
  for (index = 0; index < accounts.length; index++) {
    try {
      //pcsearch & dashboard
      let bot = await new Bot('pc')
      await bot.auth(accounts[index])
      const stats = await getStats()
      await bot.pcRun(stats, emaillinks, args.headless)
      await withDelay(2500)
      if (/1/.test(stats.level)) await getAccountStats()
      await withDelay(2500)
      await this.driver.quit()
      await withDelay(2500)
      //mobile
      if (/2/.test(stats.level)) {
        bot = await new Bot('mobile')
        await bot.auth(accounts[index])
        await bot.mobileRun(stats)
        await withDelay(2500)
        await getAccountStats()
        await withDelay(2500)
        await this.driver.quit()
        await withDelay(2500)
      }
      await logStats(`${accounts[index].email} All Done`)
    } catch (error) {
      logError(`runAll error. On ${accounts[index].email}: `+ error.name)
      logError(error)
      await this.driver.quit()
      await withDelay(2500)
    } 
  }
}

const startAt = async function (accounts, startAtAcc, emaillinks) {
  //pcsearch & dashboard
  if (args.shuffle) accounts = shuffleAccounts(accounts, 0)
  for (index = startAtAcc; index < accounts.length; index++) {
    try {
      let bot = await new Bot('pc')
      await bot.auth(accounts[index])
      const stats = await getStats()
      await bot.pcRun(stats, emaillinks, args.headless)
      await withDelay(2500)
      if (/1/.test(stats.level)) await getAccountStats()
      await withDelay(2500)
      await this.driver.quit()
      await withDelay(2500)

      //mobile
      if (/2/.test(stats.level)) {
        bot = await new Bot('mobile')
        await bot.auth(accounts[index])
        await bot.mobileRun(stats)
        await withDelay(2500)
        await getAccountStats()
        await withDelay(2500)
        await this.driver.quit()
        await withDelay(2500)
      }
      await logStats(`${accounts[index].email} All Done`)
    } catch (error) {
      logError(`startAt error. On ${accounts[index].email}: ` + error.name)
      logError(error)
      await this.driver.quit()
      await withDelay(2500)
    } 
  }  
}


const single = async function (account, emaillinks) {
  try {
    //pcsearch & dashboard
    let bot = await new Bot('pc')
    await bot.auth(account)
    const stats = await getStats()
    await bot.pcRun(stats, emaillinks, args.headless)
    await withDelay(2500)
    if (/1/.test(stats.level)) await getAccountStats()
    await withDelay(2500)
    await this.driver.quit()
    await withDelay(2500)
    //mobile
    if (/2/.test(stats.level)) {
      bot = await new Bot('mobile')
      await bot.auth(account)
      await bot.mobileRun(stats)
      await withDelay(2500)
      await getAccountStats()
      await withDelay(2500)
      await this.driver.quit()
      await withDelay(2500)
    }
    await logStats(`${account.email} All Done`)
  } catch (error) {
    logError(`single error on: ${account.email}: `+ error.name)
    logError(error)
    await this.driver.quit()
    await withDelay(2500)
  }
}

const dashboardOnly = async function (account) {
  try {
    let bot = await new Bot('pc')
    await bot.auth(account)
    await withDelay(2500)
    await bot.runDashboard(args.headless)
    await withDelay(2500)
    await this.driver.quit()
    logStats(`--Dashboard only Done.`)
    await withDelay(2500)
  } catch (error) {
    logError(`dashboardonly error on: ${account.email}: `+ error.name)
    logError(error)
    await this.driver.quit()
    await withDelay(2500)
  }
}

const mobileOnly = async function (account) {
  try {
    let bot = await new Bot('mobile')
    await this.driver.manage().window().setRect({width:375,height:667})
    await bot.auth(account)
    await bot.mobileRun(await getStats(true))
    await withDelay(2500)
    logStats(`--Mobile only Done.`)
    await this.driver.quit()
    await withDelay(2500)
  } catch (error) {
    logError(`mobileOnly error on: ${account.email}: `+ error.name)
    logError(error)
    await this.driver.quit()
    await withDelay(2500)
  }
}

const emailOnly = async function (account) {
  try {
    let bot = await new Bot('pc')
    await bot.auth(account)
    await runEmailLinks() 
  } catch (error) {
    logError(`emailOnly error on: ${account.email}: `+ error.name)
    logError(error)
    await this.driver.quit()
    await withDelay(2500)
  }
}

const test = async function (account) {
  try {
    let bot = await new Bot('pc')
    await bot.auth(account)
    await driver.get('https://account.microsoft.com/rewards/?rtc=1&refd=www.microsoft.com')
    await withDelay(2500)
    await checkForPunchcards()
    // let links = await getEmailLinks()
    // links.forEach(async function (link) {
    //   await this.driver.get(link)
    // });
    //await runEmailLinks() 
    

    // await driver.get(whatsOnTopLink)
    // await withDelay(2500)
    // await driver.navigate().refresh()
    // await driver.navigate().refresh()
    // await rewardsQuizCheck(whatsOnTopLink)

    // await withDelay(5000)
    //await rewardsQuiz()
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  runAll,
  startAt,
  single,
  dashboardOnly,
  mobileOnly,
  emailOnly,
  test
}
