const { By, Key } = require('selenium-webdriver');
const { simpleQuizCheck, pollCheck, rewardsQuizCheck, simpleQuizStart, pollStart, rewardsQuizStart, whatsOnTopStart } = require('./quiz')
const { getTab, closeTab, getNewestTab, withDelay, randSeconds, getTabsLength } = require('./helpers')
const { logStats, logError } = require('./logger')

const checkForPunchcards = async function () {
  try {
    await pausePunchcardSlide()
    let punchcards = await getPunchcardElements()
    for (let index = 0; index < punchcards.length; index++) {
      punchcards = await getPunchcardElements()
      const card = punchcards[index];
      const cardLink = await getPunchcardLink(card)
      //console.log(cardLink)
      if (!await isValidCard(cardLink)) return //console.log('not a valid card')
      if (isQuestPunchcard(cardLink) && !await isQuestActivated(card)) return await activateQuest(card)
      if (!await isCardComplete(card) && !isQuestPunchcard(cardLink)) return await completePunchcard(card)
    }
  } catch (error) {
    logError('Error encounterd whilst checking for punchcards')
    logError(error)
  }
}

const completePunchcard = async function (punchcard) {
  logStats('--Found a punchcard that needs to be completed.')
  try {
    await openPunchcard(punchcard)
    const offers = await getPunchcardOffers()
    console.log(`Found ${offers.length} punchcard offers`)
    await completePunchcardOffers(offers)
    await logPunchcardState()
    await closeTab()
  } catch (error) {
    logError('Error encountered whilst trying to complete a punchcard')
    if (await getTabsLength() > 1) await closeTab()
  }
}

const completePunchcardOffers = async function (offers) {
  for (let index = 0; index < offers.length; index++) {
    const offer = offers[index];
    try {
      if (await isPunchcardOfferComplete(offer)) return
      await openPunchcardOffer(offer)
      await checkOfferForQuiz()
      await closeTab()
    } catch (error) {
      logError('Error encountered whilst trying to complete punchcard offers.'+ error)
      if (await getTabsLength() > 2) await closeTab()
    }
  }
}

const pausePunchcardSlide = async function () {
  const pauseBtn = await driver.findElements(By.className('c-action-toggle'))
  if (pauseBtn.length) {
    if (await pauseBtn[0].isDisplayed()) await pauseBtn[0].click()
  }
}

const getPunchcardElements = async function () {
  return await driver.findElements(By.xpath('//li[contains(@ng-repeat, "item")]'))
}

const getPunchcardLink = async function (punchcard) {
  //'.//a[contains(@mee-call-to-action, "lightweight")]'
  const linkElement = await punchcard.findElement(By.xpath('.//a'))
  return await linkElement.getAttribute('href')
}

const isValidCard = async function (href) {
  //const href = await cardLink.getAttribute('href')
  if (href.match('MoviesandTV')) return false
  if (href.match('xboxapp')) return false
  if (href.match('bingmonthlyquest')) return true
  if (href.match('punchcard')) return true
  return false
}

const logPunchcardState = async function () {
  if (await isAllOffersComplete()) return logStats('--Completed Punchcard.')
  logStats('--Failed to complete punchcard.')
}

const showNextPunchcard = async function () {
  const nextBtn = await driver.findElements(By.className('c-flipper'))
  await nextBtn[1].click()
}

const getPunchcardOffers = async function () {
  return await driver.findElements(By.className('offer-cta'))
}

const isPunchcardOfferComplete = async function (offer) {
  const offerCompleteIcon = await offer.findElements(By.xpath('.//span[contains(@class, "win-icon-CheckMark")]'))
  if (!offerCompleteIcon.length) return false
  if (await offerCompleteIcon[0].isDisplayed()) return true
  return false
  //win-icon-CheckMark
}

const isAllOffersComplete = async function () {
  await driver.navigate().refresh()
  await getNewestTab()
  const offerCompleteIcons = await driver.findElements(By.className('win-icon-CheckMark'))
  const offers = await driver.findElements(By.className('offer-cta'))
  if (offerCompleteIcons.length === offers.length) return true
  return false
}

const openPunchcardOffer = async function (offer) {
  const offerLink = await offer.findElement(By.xpath('..//a'))
  await offerLink.click()
  await getNewestTab()
}

const openPunchcard = async function (punchcard) {
  while (!await isCardDisplayed(punchcard)) await showNextPunchcard()
  await punchcard.findElement(By.xpath('.//a')).click()
  await getNewestTab()
}

const isCardDisplayed = async function (punchcard) {
  return await punchcard.isDisplayed()
}

const isCardComplete = async function (punchcard) {
  const outerCircles = await punchcard.findElements(By.xpath('.//span[contains(@class, "mee-icon-StatusCircleOuter")]'))
  const checkMarks = await punchcard.findElements(By.xpath('.//i[contains(@class, "mee-icon-StatusCircleCheckmark")]'))
  if (checkMarks.length >= outerCircles.length) {
    return true
  }
  return false
}

const checkOfferForQuiz = async function () {
  const url = await driver.getCurrentUrl()
  if (await simpleQuizCheck(url)) return await simpleQuizStart()
  if (await pollCheck(url)) return await pollStart()
  if (await rewardsQuizCheck(url) === 'wot') return await whatsOnTopStart() 
  if (await rewardsQuizCheck(url) === 'rq') return await rewardsQuizStart()
}

const testScreen = function (screenshot, name) {
  require("fs").writeFile(`${name}.png`, screenshot, 'base64', function (err) {
    console.log(err);
  });
}

const isQuestPunchcard = function (cardLink) {
  if (!cardLink.match('bingmonthlyquest')) return false
  return true
}

const isQuestActivated = async function (punchcard) {
  const checkMark = await punchcard.findElements(By.className('mee-icon-StatusCircleCheckmark'))
  if (checkMark.length) return true
  return false
}

const activateQuest = async function (punchcard) {
  try {
    await openPunchcard(punchcard)
    const offers = await getPunchcardOffers()
    console.log(`Found ${offers.length} punchcard offers`)
    const offerLink = await offers[0].findElement(By.xpath('..//a'))
    await offerLink.click()
    logStats('--Activated quest punchcard.')
    await closeTab()
    await closeTab()
  } catch (error) {
    logError('Error encountered whilst activating quest punchcard'+ error)
    if (await getTabsLength() > 2) await closeTab()
    if (await getTabsLength() > 1) await closeTab()
  }
}


module.exports = { checkForPunchcards }