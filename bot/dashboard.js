const { randSeconds, withDelay, elementIsDisplayed, elementIsEnabled, getTab, getNewestTab, takeScreenshot, closeTab, ensureUrl, ensureSignedIn, acceptTerms, removeElement } = require('./helpers')
const { By } = require('selenium-webdriver');
const { logStats, logError } = require('./logger')
const { simpleQuizCheck, pollCheck, rewardsQuizCheck, pollStart, simpleQuizStart, rewardsQuizStart, whatsOnTopStart } = require('./quiz')
const { checkForPunchcards } = require('./punchcards')
const { welcomeTourCheck } = require('./welcome_tour')
const dashboardUrl = 'https://rewards.microsoft.com/Signin?idru=/?refd=www.bing.com&redref=amc'
const config = require('../config/config.json')
//https://account.microsoft.com/rewards/?ref=bingflyout another dashboard url
//https://account.microsoft.com/rewards/?refd=www.bing.com

const runOffers = async function () {
  //remove cookie cosent element it can be invis? and intercept clicks for some users
  await removeElement('cookieConsentContainer')
  await acceptTerms(By.xpath(`//*[@id="wcpConsentBannerCtrl"]/div[2]/button[1]
  `))
  let timesRan = 0
  try {
    while (timesRan < config.dashboardRuns) {
      //claim bonus points reminder!!!!
      await goDashboard()
      const offers = await getOffers(), partialOffers = await getPartialOffers()
      if (offers.length === 0 && partialOffers.length === 0) break
      console.log(`Found ${offers.length} incomplete & ${partialOffers.length} partially done offers on the dashboard.`)
      await cycleOffers(offers)
      await cycleOffers(partialOffers)
      await checkForPunchcards()      
      timesRan++
    }
    logDashboardStats(timesRan, await getOffers(), await getPartialOffers())
  } catch (error) {
    logError('runoffers error'+ error)
    logError(error)
  }
}

const logDashboardStats = function (timesRan, offers, partialOffers) {
  logStats(`--Ran Dashboard ${timesRan} times. Offers incomplete: ${offers.length}. Partially done offers: ${partialOffers.length}`)
}

const goDashboard = async function () {
  try {
    await driver.get(dashboardUrl)
    await ensureUrl(dashboardUrl, '\/rewards\/|rewards-dashboard|rewards\.microsoft')
    await withDelay(500)
  } catch (e) {
    throw new Error('Unable to reach rewards dashboard:' + e)
  }
}

const getOffers = async function () {
  return await driver.findElements(By.xpath('//span[contains(@class, "mee-icon-AddMedium")]'))
}
const getPartialOffers = async function () {
  return await driver.findElements(By.xpath('//span[contains(@class, "mee-icon-HourGlass")]'))
}

const cycleOffers = async function (offers) {
  for (let index = 0; index < offers.length; index++) {
    try {
      const offer = await offers[index].findElement(By.xpath("..//..//..//.."))
      await openOffer(offer)
      await dashboardSigninCheck(offer)
      await checkOffer()
      await closeOffer()
    } catch (error) {
      logError('cycle offer error ' + error)
      await closeOffer()
    }
  }
}

const openOffer = async function (offer) {
  await offer.click()
  await getNewestTab()
}

const closeOffer = async function () {
  const tabs = await driver.getAllWindowHandles()
  if (tabs.length > 1) await closeTab()
}
const dashboardSigninCheck = async function (offer) {
  const signInBtn = await driver.findElements(By.id('id_a'))
  if (!signInBtn.length) return
    if (await signInBtn[0].isDisplayed()) {
      await signInBtn[0].click()
      await withDelay(1200)
      await closeTab()
      await withDelay(1000)
      await openOffer(offer)
    }
}
const dashboardSigninCheck1 = async function (offer) {
  await ensureSignedIn()
  await closeTab()
  await openOffer(offer)
}

const checkOffer = async function () {
  const args = require('minimist')(process.argv.slice(2))
  const url = await driver.getCurrentUrl()
  await acceptTerms(By.id('bnp_btn_accept'))
  await welcomeTourCheck(url) // welcometour
  if (await pollCheck(url)) return await pollStart() //poll
  if (await simpleQuizCheck(url)) return await simpleQuizStart()//simeple abc quiz
  if (await rewardsQuizCheck(url) === 'rq') return await rewardsQuizStart() //rewards quiz
  if (await rewardsQuizCheck(url) === 'wot') return await whatsOnTopStart() //rewards quiz
  if (args.sweepstakes) await sweepstakes(url)
  await withDelay(randSeconds())
}

//enter sweepstakes
const sweepstakes = async function (url) {
  try {
    if (!/rewards\/redeem/.test(url)) return
    await elementIsDisplayed(By.className('btn-primary'))
    await elementIsEnabled(By.className('btn-primary'))
    enter = await driver.findElements(By.className('btn-primary'))
    await withDelay(randSeconds())
    await enter[0].click()
    await withDelay(randSeconds())
    await elementIsDisplayed(By.className('btn-primary'))
    await elementIsEnabled(By.className('btn-primary'))
    enter = await driver.findElements(By.className('btn-primary'))
    await enter[0].click()
  } catch (error) {
    logError('sweepstakes error: '+ error)
    logError(error)
  }
}





module.exports = { runOffers }