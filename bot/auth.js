const {By, Key} = require('selenium-webdriver');
const {withDelay, randSeconds, elementToExist, elementIsDisplayed, ensureUrl} = require('./helpers')
const {logStats, logError} = require('./logger')

const auth = async function (account) {
  logStats(`--${account.email} Attempting Auth...`)
  await goLogin()
  await enterEmail(account.email)
  await accountTypeCheck()
  await enterPassword(account.password)
  //Wait one second for page to load, 400 should respond in short time
  await withDelay(1)
  //Fix 400 error
  await fix400Forward()
  await lockCheck()
  await withDelay(randSeconds())
  await infoAccurateCheck()
  await updatingTerms()
  await staySignedIn()
  await protectCheck()
  logStats(`--${account.email} Auth complete.`)
}

const signInSelect = async function () {
  const pcSignBtn = await driver.findElements(By.className('identityOption'))
  if (!pcSignBtn.length) return await driver.findElement(By.id('WLSignin')).click() 
  await pcSignBtn[0].findElement(By.xpath("//a")).click()
  await withDelay(randSeconds())
}

const fix400Forward = async function(){
  //Checks if there's a error loading the page
  driver.findElement(By.id('main-frame-error')).then(function(webelement){
    driver.get("https://account.microsoft.com")
    console.log("fixed HTTP-400 mobile error by navigating to the Microsoft account page")
  },function(err) {
    console.log("no 400 error on login, proceeding...")
  });
}

const goLogin = async function () {
  await driver.get(`https://login.live.com/login.srf`)
  //await signInSelect()
  await ensureUrl('https://login.live.com/login.srf', 'login.live')
}

const enterEmail = async function (email) {
  await elementIsDisplayed(By.name('loginfmt'), 20000)
  const userField = await driver.findElement(By.name('loginfmt'))
  await withDelay(randSeconds())
  await userField.sendKeys(email, Key.RETURN)
  await usernameError()
}

const enterPassword = async function (password) {
  await elementIsDisplayed(By.name('passwd'), 20000)
  const passField = await driver.findElement(By.name('passwd'))
  await withDelay(randSeconds())
  await passField.sendKeys(password, Key.RETURN)
  await passwordError()
}

async function usernameError() {
  await elementIsDisplayed(By.id('usernameError'),2000)
   .then(element => {
     if (!element) return
     if (element) throw new Error('Incorrect username')
   }, err => {
    if (err.name === 'TimeoutError') return
  })
}

async function passwordError() {
  await elementIsDisplayed(By.id('passwordError'),2000)
    .then(element => {
      if (!element) return;
      else if (element) throw new Error('Incorrect password');
    }, err => {
      if (err.name === 'TimeoutError') return;
    })
}

const accountTypeCheck = async function ()  {
  try {
    await elementToExist(By.id('iDisambigRenameLink'),2500)
    const personalAccountBtn = await driver.findElements(By.id('msaTileTitle'))
    if (personalAccountBtn.length > 0) return await personalAccountBtn[0].click()
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('no account type check. proceeding...')
    throw new Error('account type check error: '+ error)
  }
}

const protectCheck = async function () {
  try{
    await elementToExist(By.id('iShowSkip'),2500)
    const skip = await driver.findElements(By.id('iShowSkip'))
    await withDelay(randSeconds())
    if (skip.length > 0) return await skip[0].click()
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('no protect check. proceeding...')
    throw new Error('protect check error:'+ error)
  }
}

const lockCheck = async function () {
  try {
    const lockScreen = await elementToExist(By.id('StartHeader'),2500)
    if (lockScreen) throw new Error('ACCOUNT LOCKED!')
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('account not locked, proceeding...')
    throw new Error(error)
  }
}

const infoAccurateCheck = async function () {
  try {
    await elementToExist(By.id('iRemindMeLater'), 2500)
    const remindLater = await driver.findElements(By.id('iRemindMeLater'))
    await withDelay(randSeconds())
    if (remindLater.length > 0) return await remindLater[0].click()
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('no info accurate check. proceeding...')
    throw new Error('info accurate check error:'+ error)
  }
}

const staySignedIn = async function () {
  try {
    await elementIsDisplayed(By.id('KmsiCheckboxField'), 2500)
    await driver.findElement(By.id('KmsiCheckboxField')).click()
    await driver.findElement(By.id('idSIButton9')).click()
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('no stay signed in question. proceeding...')
    throw new Error ('stay signed in question error' + error)
  }
}

const updatingTerms = async function () {
  try {
    await elementIsDisplayed(By.id('iLearnMoreLink'), 2500)
    const learnMoreLink = await driver.findElements(By.id('iLearnMoreLink'))
    if (!learnMoreLink) return
    await driver.findElement(By.className('button-container')).click()
  } catch (error) {
    if (error.name === 'TimeoutError') return console.log('no updating terms screen. proceeding...')
    throw new Error ('updating terms screen error'+ error)
  }
}

module.exports = {auth}