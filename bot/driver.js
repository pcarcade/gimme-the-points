const chrome = require('chromedriver');
const webdriver = require('selenium-webdriver');
const { Options } = require('selenium-webdriver/chrome');
const useragents = require('../config/useragents.json')
const {logError, logStats} = require('./logger')
const {userAgentFormatter} = require('./helpers')


class Driver { 
  constructor(type, headless){
   this.type = type
   this.headless = headless 
   this.options = new Options()
  }

}

Driver.prototype.setBinary = async function () {
  if (process.platform == 'win32') {
    return '/node_modules/chromedriver/lib/chromedriver.exe'
  } else {
    return '/node_modules/chromedriver/lib/chromedriver'
  }
}

Driver.prototype.createDriver = async function (driverType)  {
  const args = require('minimist')(process.argv.slice(2))
  this.options = new Options()
  //suppress browser logs in headless
  this.options.addArguments('log-level=3')
  //block notifications and geo
  if (args.disableimgs) {
    this.options.setUserPreferences({
      "profile.default_content_setting_values.notifications": 2,
      "profile.default_content_setting_values.geolocation": 2,
      "profile.managed_default_content_settings.images": 2
    });
  } else {
    this.options.setUserPreferences({
      "profile.default_content_setting_values.notifications": 2,
      "profile.default_content_setting_values.geolocation": 2
    });
  }
  
  try {
    if(args.headless) this.options.headless()
    if(driverType == 'pc') this.options.addArguments(userAgentFormatter(useragents.pc))
    if(driverType == 'mobile') this.options.addArguments(userAgentFormatter(useragents.mobile))
    return driver = new webdriver.Builder()
      .forBrowser('chrome').setChromeOptions(this.options)
      .build()
  } catch (error) {
    logError(`error creating driver:`+ error.name)
    logError(error)
  }
}

module.exports = Driver
