const git = require('simple-git/promise')
const fs = require('fs-extra')
const nodefetch = require('node-fetch')
const { exec } = require('child_process')
const readline = require('readline');
const {logStats, logError} = require('./logger')
const args = require('minimist')(process.argv.slice(2))
const backupRootFiles = ['./index.js', './package.json', './package-lock.json']
const backupDirs = ['./config', './bot']

const getRemoteVersion = async function () {
  let branch = 'master'
  if (args.updatedev) branch = 'dev'
  const url = `https://gitlab.com/NotoriousBadman/gimme-the-points/-/raw/${branch}/package.json`
  try {
    let packagejson = await nodefetch(url)
    packagejson = await packagejson.json()
    return packagejson.version
  } catch (e) {
    throw new Error('Unable to contact remote repoistory.', e)
  }
}

const getVersion = function () {
  const packagejson = fs.readFileSync('./package.json');
  return JSON.parse(packagejson).version;
}

const cloneRemote = async function () {
  console.log('Getting remote files...')
  let branch = 'master'
  if (args.updatedev) branch = 'dev'
  try {
    await fs.emptyDir('./temp/');
    await git().clone('https://gitlab.com/NotoriousBadman/gimme-the-points.git', './temp/remote', ['-b', branch])
  } catch (e) {
    throw new Error('Unable to clone remote repository.', e)
  }
}

const deleteClonedAccounts = async function () {
  try {
    fs.unlinkSync('./temp/remote/config/accounts.json')
  } catch (e) {
    throw new Error('Unable to remove cloned accounts.json', e)
  }
}

const backupWorking = async function () {
  const tempDir = './temp/backup/'
  console.log('Backing up working dir...')
  try {
    backupDirs.forEach(async dir => {
      fs.copySync(dir, tempDir + dir)
    });
    backupRootFiles.forEach(file => {
      fs.copyFileSync(file, tempDir + file)
    });
  } catch (e) {
    throw new Error('Unable to backup working dir', e)
  }
}

async function copyRemoteToWorking() {
  console.log('Copying update...')
  try {
    fs.copySync('./temp/remote', './')
  } catch (e) {
    throw new Error('Unable to copy update'+ error)
  }
}

const checkForUpdate = async function () {
  //if (await isUpdateAvaliable()) await updatePrompt()
  if (await isUpdateAvaliable()) await createUpdatePrompt()
}

const getUpdate = async function () {
  await cloneRemote()
  await backupWorking()
  await deleteClonedAccounts()
  await copyRemoteToWorking()
  console.log('Running npm install...')
  exec('npm install')
  console.log('UPDATED FILES.  THE PROCESS WILL NOW EXIT..')
  exec('pause')
  process.exit();
}

function versionCompare(v1, v2, options) {
  var lexicographical = options && options.lexicographical,
      zeroExtend = options && options.zeroExtend,
      v1parts = v1.split('.'),
      v2parts = v2.split('.');

  function isValidPart(x) {
      return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
  }

  if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
      return NaN;
  }

  if (zeroExtend) {
      while (v1parts.length < v2parts.length) v1parts.push("0");
      while (v2parts.length < v1parts.length) v2parts.push("0");
  }

  if (!lexicographical) {
      v1parts = v1parts.map(Number);
      v2parts = v2parts.map(Number);
  }

  for (var i = 0; i < v1parts.length; ++i) {
      if (v2parts.length == i) {
          return 1;
      }

      if (v1parts[i] == v2parts[i]) {
          continue;
      }
      else if (v1parts[i] > v2parts[i]) {
          return 1;
      }
      else {
          return -1;
      }
  }

  if (v1parts.length != v2parts.length) {
      return -1;
  }

  return 0;
}


const isUpdateAvaliable = async function () {
  const currentVersion = getVersion()
  const remoteVersion = await getRemoteVersion()
  try {
    if (versionCompare(remoteVersion, currentVersion) == 1) {
      logStats(`There is an update available. Current version:${currentVersion} Remote version:${remoteVersion}`)
      return true
    }
    return false
  } catch (e) {
    logError('Unable to check for update.', e)
  }
}

const updatePrompt = function (rl) {
  var response;
  rl.setPrompt('Enter 1 to update or any other key to cancel.\nWill cancel in 10 seconds.\n');
  rl.prompt();

  return new Promise((resolve, reject) => {
    rl.on('line', async (userInput) => {
      response = userInput;
      rl.close();
    });

    rl.on('close', () => {
      resolve(response);
    });

    setTimeout(() => {
      reject('Proceeding without update.')
    }, 10000);
  });
};

const createInterface = function () {
  return readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
}

const createUpdatePrompt = async function () {
  const rl = createInterface()
  try {
    const userResponse = await updatePrompt(rl)
    if (userResponse === '1') await getUpdate()
  } catch (e) {
    console.log(e)
    rl.write(null, { ctrl: false, name: 'Return' })
  }
}

const autoUpdate = async function () {
  if (await isUpdateAvaliable()) await getUpdate()

}

module.exports = { checkForUpdate, autoUpdate }